package ru.vkandyba.tm.exception.entity;

import ru.vkandyba.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found!");
    }

}
