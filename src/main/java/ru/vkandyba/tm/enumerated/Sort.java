package ru.vkandyba.tm.enumerated;

import ru.vkandyba.tm.comparator.ComparatorByCreated;
import ru.vkandyba.tm.comparator.ComparatorByName;
import ru.vkandyba.tm.comparator.ComparatorByStartDate;
import ru.vkandyba.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created date", ComparatorByCreated.getInstance()),
    START_DATE("Sort by start date", ComparatorByStartDate.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }
}
