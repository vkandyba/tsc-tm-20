package ru.vkandyba.tm.repository;

import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public Boolean existsByIndex(Integer index) {
        if (index < 0) return false;
        return index < list.size();
    }

    @Override
    public Boolean existsById(String id) {
        final E entity = findById(id);
        return entity != null;
    }

    @Override
    public E findById(String id) {
        for (E entity : list) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(Integer index) {
        return list.get(index);
    }

    @Override
    public E removeById(String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(Integer index) {
        final E entity = findByIndex(index);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

    @Override
    public E add(E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(E entity) {
        list.remove(entity);
    }

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        final List<E> entityList = new ArrayList<>(list);
        entityList.sort(comparator);
        return entityList;
    }

}
