package ru.vkandyba.tm.repository;

import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public Task findByName(String userId, String name) {
        for (Task task : list) {
            if (name.equals(task.getName()) && userId.equals(task.getUserId())) return task;
        }
        return null;
    }

    @Override
    public Task removeByName(String userId, String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task startById(String userId, String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(String userId, Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(String userId, String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(String userId, String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(String userId, Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(String userId, String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusById(String userId, String id, Status status) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(String userId, Integer index, Status status) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(String userId, String name, Status status) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task bindTaskToProjectById(String userId, String projectId, String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskToProjectById(String userId, String projectId, String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(String userId, String projectId) {
        List<Task> listByProject = new ArrayList<>();
        List<Task> taskList = findAll(userId);
        for (Task task : taskList) {
            if (projectId.equals(task.getProjectId())) listByProject.add(task);
        }
        return listByProject;
    }

    @Override
    public void removeAllTaskByProjectId(String userId, String projectId) {
        List<Task> listByProject = findAllTaskByProjectId(userId, projectId);
        for (Task task : listByProject) {
            list.remove(task);
        }
    }

    @Override
    public void clear(String userId) {
        list.removeAll(findAll(userId));
    }

}
