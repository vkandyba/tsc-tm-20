package ru.vkandyba.tm.repository;

import ru.vkandyba.tm.api.repository.IBusinessRepository;
import ru.vkandyba.tm.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public Boolean existsByIndex(String userId, Integer index) {
        if (index < 0) return false;
        final List<E> entityList = findAll(userId);
        return index < entityList.size();
    }

    @Override
    public Boolean existsById(String userId, String id) {
        final E entity = findById(id);
        return entity != null;
    }

    @Override
    public E findById(String userId, String id) {
        for (E entity : list) {
            if (id.equals(entity.getId()) && userId.equals(entity.getUserId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(String userId, Integer index) {
        final List<E> entityList = findAll(userId);
        return entityList.get(index);
    }

    @Override
    public E removeById(String userId, String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(String userId, Integer index) {
        final E entity = findByIndex(index);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

    @Override
    public E add(String userId, E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(String userId, E entity) {
        list.remove(entity);
    }

    @Override
    public List<E> findAll(String userId) {
        final List<E> entityList = new ArrayList<>();
        for (E entity : list) {
            if (userId.equals(entity.getUserId())) entityList.add(entity);
        }
        return entityList;
    }

    @Override
    public List<E> findAll(String userId, Comparator<E> comparator) {
        final List<E> entityList = findAll(userId);
        entityList.sort(comparator);
        return entityList;
    }

}
