package ru.vkandyba.tm.command.task;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.exception.entity.TaskNotFoundException;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-show-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(userId, id);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        System.out.println("Index: " + tasks.indexOf(task));
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus());
    }

}
