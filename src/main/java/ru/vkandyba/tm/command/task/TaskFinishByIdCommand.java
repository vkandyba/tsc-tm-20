package ru.vkandyba.tm.command.task;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.exception.entity.TaskNotFoundException;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.util.TerminalUtil;

public class TaskFinishByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish task by id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishById(userId, id);
        if (task == null) throw new TaskNotFoundException();
    }

}
