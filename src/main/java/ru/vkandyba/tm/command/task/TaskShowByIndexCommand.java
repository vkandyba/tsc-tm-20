package ru.vkandyba.tm.command.task;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.exception.entity.TaskNotFoundException;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowByIndexCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-show-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by index...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!serviceLocator.getTaskService().existsByIndex(userId, index)) {
            throw new TaskNotFoundException();
        }
        final Task task = serviceLocator.getTaskService().findByIndex(userId, index);
        final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        System.out.println("Index: " + tasks.indexOf(task));
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus());
    }

}
