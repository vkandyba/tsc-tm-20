package ru.vkandyba.tm.command.task;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.exception.entity.TaskNotFoundException;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskChangeStatusByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-change-status-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change status task by id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = serviceLocator.getTaskService().changeStatusById(userId, id, status);
        if (task == null) throw new TaskNotFoundException();
    }

}
