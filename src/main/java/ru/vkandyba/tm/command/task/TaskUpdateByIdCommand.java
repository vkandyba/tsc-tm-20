package ru.vkandyba.tm.command.task;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.exception.entity.TaskNotFoundException;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        if (serviceLocator.getTaskService().existsById(userId, id)) {
            throw new TaskNotFoundException();
        }
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().updateById(userId, id, name, description);
        if (task == null) {
            throw new TaskNotFoundException();
        } else {
            System.out.println("[OK]");
        }
    }

}
