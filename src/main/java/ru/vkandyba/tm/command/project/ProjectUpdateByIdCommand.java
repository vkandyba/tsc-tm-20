package ru.vkandyba.tm.command.project;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.exception.entity.ProjectNotFoundException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update project by id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().existsById(userId, id)) {
            throw new ProjectNotFoundException();
        }
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().updateById(userId, id, name, description);
        if (project == null) {
            throw new ProjectNotFoundException();
        } else {
            System.out.println("[OK]");
        }
    }

}
