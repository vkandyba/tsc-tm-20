package ru.vkandyba.tm.model;

import ru.vkandyba.tm.api.entity.IWBS;
import ru.vkandyba.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class Project extends AbstractBusinessEntity implements IWBS {

    private String name;

    private String description;

    private Status status = Status.NON_STARTED;

    private Date startDate;

    private Date finishDate;

    private Date createdDate = new Date();

    public Project() {

    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public Date getCreated() {
        return createdDate;
    }

    @Override
    public void setCreated(Date created) {
        this.createdDate = created;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

}
