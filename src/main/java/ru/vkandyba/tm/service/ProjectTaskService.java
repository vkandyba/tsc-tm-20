package ru.vkandyba.tm.service;

import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.service.IProjectService;
import ru.vkandyba.tm.api.service.IProjectTaskService;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(String userId, String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    public Task bindTaskToProjectById(String userId, String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        if (!taskRepository.existsById(taskId)) return null;
        return taskRepository.bindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    public Task unbindTaskToProjectById(String userId, String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        if (!taskRepository.existsById(taskId)) return null;
        return taskRepository.unbindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    public void removeAllTaskByProjectId(String userId, String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.removeAllTaskByProjectId(userId, projectId);
    }

    @Override
    public Project removeById(String userId, String projectId) {
        removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(projectId);
    }

}
