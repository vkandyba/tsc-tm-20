package ru.vkandyba.tm.service;

import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.api.service.IService;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyIndexException;
import ru.vkandyba.tm.model.AbstractEntity;
import ru.vkandyba.tm.repository.AbstractRepository;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public Boolean existsByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.existsByIndex(index);
    }

    @Override
    public Boolean existsById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existsById(id);
    }

    @Override
    public E findById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public E findByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findByIndex(index);
    }

    @Override
    public E removeById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public E removeByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.removeByIndex(index);
    }

    @Override
    public E add(E entity) {
        if (entity == null) throw new RuntimeException();
        return repository.add(entity);
    }

    @Override
    public void remove(E entity) {
        if (entity == null) throw new RuntimeException();
        repository.remove(entity);
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        return repository.findAll(comparator);
    }
}
