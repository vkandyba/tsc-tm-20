package ru.vkandyba.tm.service;

import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.service.IProjectService;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyIndexException;
import ru.vkandyba.tm.exception.empty.EmptyNameException;
import ru.vkandyba.tm.model.Project;

public class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public Project findByName(String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @Override
    public Project removeByName(String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(userId, name);
    }

    @Override
    public Project updateByIndex(String userId, Integer index, String name, String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(String userId, String id, String name, String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findById(userId, id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.startById(userId, id);
    }

    @Override
    public Project startByIndex(String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.startByIndex(userId, index);
    }

    @Override
    public Project startByName(String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.startByName(userId, name);
    }

    @Override
    public Project finishById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.finishById(userId, id);
    }

    @Override
    public Project finishByIndex(String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.finishByIndex(userId, index);
    }

    @Override
    public Project finishByName(String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.finishByName(userId, name);
    }

    @Override
    public Project changeStatusById(String userId, String id, Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) return null;
        return projectRepository.finishById(userId, id);
    }

    @Override
    public Project changeStatusByIndex(String userId, Integer index, Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new RuntimeException("Error! Status is empty!");
        return projectRepository.finishByIndex(userId, index);
    }

    @Override
    public Project changeStatusByName(String userId, String name, Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new RuntimeException("Error! Status is empty!");
        return projectRepository.finishByName(userId, name);
    }

    @Override
    public void create(String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new RuntimeException("Error! Description is empty!");
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public void clear(String userId) {
        projectRepository.clear(userId);
    }

}
